/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendas;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
/**
 *
 * @author Marlon dos Santos
 * @obs.: Sei que nao havia necessidade em usar JSON, pois poderia exibir direto das proprias variaveis que o 
 * abastece, porem quis fazer desse jeito para aprender sua utilizacao
 */
public class Vendas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        FileReader fr = null;
        BufferedReader br = null;
        String arquivo = "C:\\Users\\marlo\\OneDrive\\Documents\\NetBeansProjects\\Semana1\\Vendas\\src\\vendas\\vendas.txt";
              
        double Total_filial_1 = 0, Total_filial_2 = 0, Total_filial_3 = 0, Total_filial_4 = 0;
        double Media_filial_1 = 0, Media_filial_2 = 0, Media_filial_3 = 0, Media_filial_4 = 0;
        int lenghtF1 = 0, lenghtF2 = 0, lenghtF3 = 0, lenghtF4 = 0; // qtd de vendas
        
        DecimalFormat df_total = new DecimalFormat();
        df_total.applyPattern("Total: R$ #,##0.00");

        DecimalFormat df_media = new DecimalFormat();
        df_media.applyPattern("Media: R$ #,##0.00");

        try {
            fr = new FileReader(arquivo);
            br = new BufferedReader(fr);
            String linha = br.readLine();
            
            // PERCORRE O ARQUIVO - LINHA POR LINHA
            while (linha != null) {
              // System.out.println(linha);
              // sobrepoem valor caso ja tenha mesma key - total.put("Filial_" + linha.substring(0, linha.indexOf(",")), linha.substring(linha.indexOf(",")+1) ); 
              // dobro do valor - total.accumulate("Filial_" + linha.substring(0, linha.indexOf(",")), total.getDouble("Filial_" + linha.substring(0, linha.indexOf(","))) + Double.parseDouble(linha.substring(linha.indexOf(",")+1)) );

              // VERIFICA QUAL FILIAL PERTENCE A VENDA DE CADA LINHA E ARMAZENA NO JSON
              if(linha.substring(0, linha.indexOf(",")).equals("1")){
                Total_filial_1 += Double.parseDouble(linha.substring(linha.indexOf(",")+1));
                lenghtF1++;
                
              }else if(linha.substring(0, linha.indexOf(",")).equals("2")){
                Total_filial_2 += Double.parseDouble(linha.substring(linha.indexOf(",")+1));
                lenghtF2++;
                
              }else if(linha.substring(0, linha.indexOf(",")).equals("3")){
                Total_filial_3 += Double.parseDouble(linha.substring(linha.indexOf(",")+1));
                lenghtF3++;
                
              }else if(linha.substring(0, linha.indexOf(",")).equals("4")){
                Total_filial_4 += Double.parseDouble(linha.substring(linha.indexOf(",")+1));
                lenghtF4++;
              }
              
               linha = br.readLine();
            }
            
            // CALCULA A MÉDIA DE CADA FILIAL
            Media_filial_1 = Total_filial_1 / lenghtF1;
            Media_filial_2 = Total_filial_2 / lenghtF2;
            Media_filial_3 = Total_filial_3 / lenghtF3;
            Media_filial_4 = Total_filial_4 / lenghtF4;

        } catch (FileNotFoundException e) {
            System.out.println(arquivo + " não existe.");
        } catch (IOException e) {
            System.out.println("Erro na leitura do arquivo.");
        } finally {
            br.close();
            fr.close();
           // System.out.println(vendaPorFilial);
           
           // SAIDA DE RESULTADOS
            System.out.println("FILIAL 1" + "\n"
                    + "------------------\n"
                    + "Total: " + df_total.format(Total_filial_1) + "\n"
                    + "Qtd Vendas: " + lenghtF1 + "\n"
                    + "Média: " + df_media.format(Media_filial_1) + "\n"
                    + "==================\n"
                    + "Filial 2" + "\n"
                     + "------------------\n"
                    + "Total: " + df_total.format(Total_filial_2) + "\n"
                    + "Qtd Vendas: " + lenghtF2 + "\n"
                    + "Média: " + df_media.format(Media_filial_2) + "\n"
                    + "==================\n"
                    + "Filial 3" + "\n"
                     + "------------------\n"
                    + "Total: " + df_total.format(Total_filial_3) + "\n"
                    + "Qtd Vendas: " + lenghtF3 + "\n"
                    + "Média: " + df_media.format(Media_filial_3) + "\n"
                    + "==================\n"
                    + "Filial 4" + "\n"
                     + "------------------\n"
                    + "Total: " + df_total.format(Total_filial_4) + "\n"
                    + "Qtd Vendas: " + lenghtF4 + "\n"
                    + "Média:" + df_media.format(Media_filial_4) + "\n"
                    + "==================\n"
            );
        }
    }
}
