/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package criandoarquivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author marlo
 */
public class CriandoArquivos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        try{
        Scanner sc = new Scanner(System.in);
        String fileName = new String();
        String inp = new String();
        
        System.out.println("Informe o nome do arquivo: ");
        fileName = sc.next();
        
        System.out.println("Informe os argumentos: ");
        inp = sc.next(); 
        inp += sc.nextLine();
                
       // System.out.println(inp);
        System.out.println();
        
        FileWriter fw = null;
        PrintWriter saida = null;
        String caminho = "C:\\Users\\marlo\\OneDrive\\Documents\\NetBeansProjects\\Semana1\\CriandoArquivos\\" + fileName + ".txt";

        FileReader fr = null;
        BufferedReader br = null;
        
        File file = new File(caminho);

        if(file.exists()){
            System.out.println("Arquivo já exite!");
            
            fr = new FileReader(caminho);
            br = new BufferedReader(fr);
            String linha = new String();
            linha = br.readLine();
            
            String conteudo = new String();
            
            while (linha != null) {
                conteudo += linha + "\n";
                linha = br.readLine();
            }
            
            fw = new FileWriter(caminho);
            saida = new PrintWriter(fw);
           
            saida.print( conteudo + inp); 
            System.out.println("Dados inseridos.");


        }else{
            System.out.println("Arquivo não existe!");
            System.out.println("Arquivo criado!");
            
            fw = new FileWriter(caminho);
            saida = new PrintWriter(fw);
            saida.append(inp); 
            System.out.println("Dados inseridos.");
        }
        
            
            saida.close();
            fw.close();
        }catch(IOException io){
            System.out.println("Erro no aquivo! Erro: " + io.getMessage());
        }    
            
        
        
    }
    
}
