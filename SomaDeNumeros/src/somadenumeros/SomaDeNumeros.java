/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package somadenumeros;

import java.util.Scanner;

/**
 *
 * @author Marlon dos Santos
 */
public class SomaDeNumeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int input = 0;
        int somador = 0;
        Scanner sc = new Scanner(System.in);
        
        do{
            System.out.println("Informe um número (-1 para sair): ");
            somador += input;
            input = sc.nextInt();
        }while(input != -1);
        
        System.out.println("A soma é: " + somador);
    }
    
}
