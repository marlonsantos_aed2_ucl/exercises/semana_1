/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraaritmetica;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Marlon dos Santos
 */
public class CalculadoraAritmetica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String option = new String();
        Integer num1, num2;
        double result = 0;
        String diviResult = new String();
        diviResult = "";
        boolean show = true;

        
        option = menu();
        // fazer tratamento pois caso selecione opcao invalida ele continua 
        
        while (!option.equals("5")) {

            System.out.println("Informe o 1º número: ");
            num1 = sc.nextInt();

            System.out.println("Informe o 2º número: ");
            num2 = sc.nextInt();

            clear();
            switch (option) {
                case "1":
                    result = Calcular.mult(num1, num2);
                    show = true;
                    break;

                case "2":
                    diviResult = Calcular.divi(num1, num2);
                    show = false;
                    break;

                case "3":
                    result = Calcular.soma(num1, num2);
                    show = true;
                    break;

                case "4":
                    result = Calcular.subt(num1, num2);
                    show = true;
                    break;

                case "5":
                    show = false;
                    break;

                default:
                    show = false;
                    break;
            }
            
            if (show) {
                System.out.println("Resultado: " + result);
            }else{
                System.out.println("Resultado: " + diviResult);
            }
            option = menu();
        }
    }
    
    public static String menu(){
        Scanner sc = new Scanner(System.in);
        
        while (true){
            System.out.println("========================");
            System.out.println("1 - Multiplicação de Inteiros");
            System.out.println("2 - Divisão de Inteiros");
            System.out.println("3 - Soma de Inteiros");
            System.out.println("4 - Subtração de Inteiros");
            System.out.println("5 - Sair");
            System.out.println("========================");
            System.out.println();
            System.out.println("Escolha uma opção: ");
            String inp = sc.next();
            
            Integer val = Integer.parseInt(inp);
            if(val > 0 && val < 6){
                return inp;
            }else{
                clear();
            }
                System.out.println("Escolha inválida, tente novamente!");
            }
        }
    
    public static void clear(){
        for(int i=0; i<100; i++){
            System.out.println("\n");
        }
    }
}
