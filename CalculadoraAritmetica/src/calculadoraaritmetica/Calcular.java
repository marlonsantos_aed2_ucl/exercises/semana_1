/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadoraaritmetica;

/**
 *
 * @author Marlon dos Santos
 */
public class Calcular {
    
    private static double result;

    public static double mult(Integer num1 , Integer num2){
        result = num1 * num2;
        return result;
    }
    
    public static String divi(Integer num1 , Integer num2){
        String result = "";
        if(num2 == 0){
            result = "Impossível dividir por 0, escolha novamente!";
        }else{
            double div = (double) num1 / num2;
            result = String.valueOf(div);
        }
        return result; 
    }
      
        public static double soma(Integer num1 , Integer num2){
        result = num1 + num2;
        return result;
    }
        
    public static double subt(Integer num1 , Integer num2){
        result = num1 - num2;
        return result;
    }
}