/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lampadas;

/**
 *
 * @author marlo
 */
public class Lampadas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        // PARA ESSA ATIVIDADE DEFINI: 0 = APAGADA , 1 = ACESA E 2 = MEIA-LUZ
        
        Lampada primeiraLampada = new Lampada();
        primeiraLampada.Apagar();
        
        Lampada segundaLampada = new Lampada("1");
        
        Lampada terceiraLampada = new Lampada();
        terceiraLampada.setEstado("2");
        
        System.out.println("A primeira lâmpada está " + primeiraLampada.getEstado());
        System.out.println("A segunda lâmpada está " + segundaLampada.getEstado());
        System.out.println("A terceira lâmpada está " + terceiraLampada.getEstado());
     }
    
    
}
