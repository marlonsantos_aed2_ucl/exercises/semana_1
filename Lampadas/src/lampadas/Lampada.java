/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lampadas;

/**
 *
 * @author marlo
 */
public class Lampada {
    String estado;

    public void Ascender(){
        this.estado = "acesa";
    }
    
    public void AscenderMeiaLuz(){
        this.estado = "meia-luz";
    }
    
    public void Apagar(){
        this.estado = "apagada";
    }

    // CONSTRUCTORS
    public Lampada(String estado) {
        if(estado == "0"){
            Apagar();
        }else if(estado == "1"){
            Ascender();
        }else if(estado == "2"){
            AscenderMeiaLuz();
        }
    }
    
    public Lampada(){
        
    }
    
    //GET AND SET
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        if(estado == "0"){
            Apagar();
        }else if(estado == "1"){
            Ascender();
        }else if(estado == "2"){
            AscenderMeiaLuz();
        }
    }
    
    
}