/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadorasimplesmetodos;

import java.text.DecimalFormat;

/**
 *
 * @author marlo
 */
public class CalculadoraSimples {
    private double number1 = 0;
    private double number2 = 0;
    private char operator = '+';
   
    private double resultado;
    private String msg = null;
    
    public void Calcular(){
        switch(this.operator){
            case '+':
                resultado = number1 + number2;
                break;
                
            case '-':
                resultado = number1 - number2;
                break;
                
            case '/':
                if(number2 == 0){
                    msg = "Impossível dividir por 0, escolha novamente!";
                }else{
                    resultado = number1 / number2;
                }
                break;
                
            case '*':
                resultado = number1 * number2;
                break;
                
            default:
                msg = "Operador inválido, tente novamente!";
                break;
        }
    }
    
    public void ExibirResultado(){
        DecimalFormat df = new DecimalFormat();
        df.applyPattern("#,##0.00");
        
        if(msg == null)
            System.out.println(df.format(resultado));
        else
            System.out.println(msg);
    }
    

    // Constructors
    public CalculadoraSimples(){
        
    }
    
    public CalculadoraSimples(double num1, double num2, char op){
        this.number1 = num1;
        this.number2 = num2;
        this.operator = op;
    }
    
    // Get and Set
    public double getNumber1() {
        return number1;
    }

    public void setNumber1(double number1) {
        this.number1 = number1;
    }

    public double getNumber2() {
        return number2;
    }

    public void setNumber2(double number2) {
        this.number2 = number2;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char operator) {
        this.operator = operator;
    }
    
    
    
}
